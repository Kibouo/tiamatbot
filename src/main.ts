import { Bot } from './bot';
import { ConfigReader } from './config';
import { Database } from './database';

function main() {
    let bot: Bot;
    process.on('SIGINT', () => bot?.stop());

    const config = ConfigReader.read();
    bot = new Bot(config, new Database(config));
}

main();