import express from 'express';

export default class WebServer {
    constructor(port: number) {
        const PORT = port || 3000;
        const app = express();

        app.get('/', (req, res) => {
            res.send(
                '<img src="https://vignette.wikia.nocookie.net/fategrandorder/images/c/cf/Tiamat_april_fool.png">');
        });
        app.listen(PORT, () => console.log(`Listening on ${PORT}`));
    }
}