import { Client } from 'discord.js';
import { Config } from './config';
import { Database } from './database';
import * as handlers from './discord/handler';

class Bot {
    private config: Config;
    private db_client: Database;
    private discord_client: Client;

    constructor(config: Config, db_client: Database) {
        this.config = config;
        this.db_client = db_client;
        this.discord_client = new Client();

        this.register_handlers();

        this.discord_client.login(this.config.token)
            .catch(err => console.error(err));
    }

    public stop() {
        this.discord_client.destroy();
        this.db_client.close();
    }

    private register_handlers() {
        this.discord_client.on(
            'ready',
            () => handlers.ready_handler(this.discord_client, this.config)
        );
        this.discord_client.on(
            'message',
            msg => handlers.message_handler(msg, this.config.prefix, this.db_client)
                .catch(err => console.error(err))
        );
        this.discord_client.on(
            'guildMemberAdd',
            member => handlers.server_join_handler(member, this.db_client)
                .catch(err => console.error(err))
        );
    }
}

export { Bot };