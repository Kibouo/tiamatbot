import { RedisClient, createClient } from 'redis';
import { Config } from './config';
import QuickLRU = require('quick-lru');

const PREFIX = "TIAMAT_DB:";
const WELCOME_CHANNEL_ID_KEY = PREFIX + "WELCOME_CHANNEL_ID_KEY";
const WELCOME_MESSAGE_KEY = PREFIX + "WELCOME_MESSAGE_KEY";
const CMD_KEY = PREFIX + "CMD_KEY_";

const LRU_CACHE_SIZE = 256;

class Database {
    private instance: RedisClient;
    private command_cache: QuickLRU<string, string[]>;
    private welcome_channel: string;
    private welcome_message: string;

    constructor(config: Config) {
        this.instance = createClient(config.db_url);
        this.instance.on('ready', () => console.log(`Connected to database ${config.db_url}`));

        this.command_cache = new QuickLRU({ maxSize: LRU_CACHE_SIZE });
    }

    public async get_welcome_channel_id(): Promise<string> {
        const cached = this.welcome_channel;

        return new Promise<string>((resolve, reject) => {
            if (cached) {
                resolve(cached);
            }
            this.instance.get(WELCOME_CHANNEL_ID_KEY, (error, result) => {
                if (error) {
                    reject(error);
                }
                this.welcome_channel = result;
                resolve(result);
            });
        });
    }

    public async set_welcome_channel_id(channel_id: string) {
        return new Promise<void>((resolve, reject) => {
            this.instance.set(WELCOME_CHANNEL_ID_KEY, channel_id, error => {
                if (error) {
                    reject(error);
                }
                this.welcome_channel = channel_id;
                resolve();
            });
        });
    }

    public async get_welcome_message(): Promise<string> {
        const cached = this.welcome_message;

        return new Promise<string>((resolve, reject) => {
            if (cached) {
                resolve(cached);
            }
            this.instance.get(WELCOME_MESSAGE_KEY, (error, result) => {
                if (error) {
                    reject(error);
                }
                this.welcome_message = result;
                resolve(result);
            });
        });
    }

    public async set_welcome_message(message: string) {
        return new Promise<void>((resolve, reject) => {
            this.instance.set(WELCOME_MESSAGE_KEY, message, error => {
                if (error) {
                    reject(error);
                }
                this.welcome_message = message;
                resolve();
            });
        });
    }

    public async get_custom_command(command: string): Promise<string[]> {
        const cached = this.command_cache.get(command);

        return new Promise<string[]>((resolve, reject) => {
            if (cached) {
                resolve(cached);
            }
            this.instance.smembers(CMD_KEY + command, (error, result) => {
                if (error) {
                    reject(error);
                }
                this.command_cache.set(command, result);
                resolve(result);
            });
        });
    }

    public async get_all_custom_commands(): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            this.instance.keys(CMD_KEY + '*', (error, result) => {
                if (error) {
                    reject(error);
                }
                resolve(result.map(key => key.replace(CMD_KEY, '')));
            });
        });
    }

    public async set_custom_command(command: string, reply: string) {
        return new Promise<void>((resolve, reject) => {
            this.instance.sadd(CMD_KEY + command, reply, error => {
                if (error) {
                    reject(error);
                }

                const cached = this.command_cache.get(command);
                if (cached) {
                    cached.push(reply);
                    this.command_cache.set(command, cached);
                } else {
                    this.command_cache.set(command, [reply]);
                }

                resolve();
            });
        });
    }

    public async delete_custom_command(command: string) {
        return new Promise<void>((resolve, reject) => {
            this.instance.del(CMD_KEY + command, error => {
                if (error) {
                    reject(error);
                }
                this.command_cache.delete(command);
                resolve();
            });
        });
    }

    public close() { this.instance.quit(); }
}

export { Database };