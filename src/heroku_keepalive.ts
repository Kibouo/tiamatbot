import fetch from 'node-fetch';

// Heroku's free dyno sleeps after ~5min. To keep it running, we gotta ping the
// app from time to time.
export default function start_keep_alive(heroku_url: string) {
    setInterval(() => {
        fetch(heroku_url)
            .then(res => {
                if (res.ok) {    // res.status >= 200 && res.status < 300
                    return res;
                }
                else {
                    return Promise.reject(new Error("Failed to send keep-alive request"));
                }
            }).then(res => {
                console.log('Keep alive ping received');
            }).catch(console.log);
    }, 4 * 60 * 1000);    // ping every 4 minutes
}