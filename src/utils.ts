import { GuildMember, PartialGuildMember, Message } from "discord.js";
import { parse as url_parse, UrlWithStringQuery } from "url";
import { extname } from "path";
import { getType as mime_type } from "mime";

const ALLOWED_MIME_CATEGORIES = ["audio", "video", "image"];

// Parse space-separated list of arguments. Multi-word parameters are possible by placing
// `"` around the words. Escaping of `"` is also possible.
function parse_params(data: string): string[] {
    return data
        .match(/(?:[^"])(?:\\"|[^"\s])*[^"]?|"(?:\\"|[^"])*"/gu) // Main parsing
        ?.map(str => {
            return str
                .replace(/(?:([^\\]|^)("))/gu, '$1') // Remove `"` from multi-word params
                .replace(/(?:\\(.))/gu, '$1') // Remove the `\` used for escaping
                .trim(); // Remove ` ` from between params
        })
        .filter(match => match !== "") || [""]; // Image messages have no text so can't match
}

function attachments_as_params(msg: Message): string[] {
    return msg.attachments
        .map(attachment => attachment.attachment)
        .filter(attachment => is_string(attachment)) as string[];
}

// Replace $user with a user ping
function replace_user_mention(text: string, user: GuildMember | PartialGuildMember) {
    return text
        ?.replace(
            /(?:([^\\]|^)(\$user))/gu,
            (_, not_backslash, __) => not_backslash + `${user}` // don't replace captured non-\
        ) || [""];
}

// So Discord has 2 ways to represent tags. For desktop app: <@!18-digit-nr>.
// For mobile app: <@18-digit-nr>. Even if you ping the same person/role,
// the actual tags are not the same.
// Basically, we remove all `!`s from tags to ensure everyone can put pings in their commands.
function clean_user_tag(text: string): string {
    return text.replace(/(?:(<@!\d{18}>))/u, (selected) => selected.split('!').join(''));
}

function extract_file_urls(text: string): [string, string[]] {
    const urls = text.match(/https?:\/\/\S+/gu) || [];

    const file_urls = urls.map(url => [url, url_parse(url)])
        .filter(([_, parsed_url]: [string, UrlWithStringQuery]) => has_file_ext(parsed_url))
        .filter(([_, parsed_url]: [string, UrlWithStringQuery]) => is_media_ext(parsed_url))
        .map(([str_url, _]: [string, UrlWithStringQuery]) => str_url);

    file_urls.forEach(url => text = text.replace(url, ''));

    // Remove spaces left over from around the urls
    return [text.trim().replace(/  +/gu, ' '), file_urls];
}

function is_media_ext(url: UrlWithStringQuery): boolean {
    const url_mime_type = mime_type(extname(url.pathname || '')) || "";
    const url_mime_category = url_mime_type.split('/')[0];
    return ALLOWED_MIME_CATEGORIES.filter(category => category === url_mime_category).length > 0;
}

function has_file_ext(url: UrlWithStringQuery): boolean {
    return extname(url.pathname || '').length > 0;
}

function random_int(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
}

function is_string(data: any): boolean {
    return typeof data === 'string' || data instanceof String;
}

enum EmbedColor {
    Default = "#bdd6dd",
    Good = "#00ff00",
    Bad = "#ff0000"
}

export { parse_params, EmbedColor, replace_user_mention, random_int, attachments_as_params, extract_file_urls as extract_urls, clean_user_tag };