import { readFileSync } from 'fs';

interface Config {
    db_url: string;
    token: string;
    prefix: string;
    port?: number;
    heroku_url?: string;
}

abstract class ConfigReader {
    public static read(): Config {
        try {

            return JSON.parse(readFileSync('./config.json', 'utf8'));
        } catch (err) {
            if (err.code === 'ENOENT') {
                console.log('config.json file not found. Reading from env...');
                return {
                    db_url: process.env.DB_URL,
                    token: process.env.TOKEN,
                    prefix: process.env.PREFIX,
                    port: Number(process.env.PORT),
                    heroku_url: process.env.HEROKU_URL
                };
            } else {
                throw err;
            }
        }
    }
}

export { Config, ConfigReader };