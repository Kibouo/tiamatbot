import { Client, Message, GuildMember, TextChannel, PartialGuildMember, Permissions } from "discord.js";
import { Database } from '../database';
import { parse_params, replace_user_mention, attachments_as_params, clean_user_tag } from '../utils';
import * as cmd from "./command";
import WebServer from "../webserver";
import { Config } from "../config";
import start_keep_alive from "../heroku_keepalive";

function ready_handler(client: Client, config: Config) {
    console.log(`Logged in as ${client.user.tag}.`);
    const webserver = new WebServer(config.port);
    start_keep_alive(config.heroku_url);
}

async function message_handler(msg: Message, cmd_prefix: string, db_client: Database) {
    // Prevent infinite loops of bot answering itself
    if (msg.author.tag === msg.client.user.tag) return;

    const msg_content = clean_user_tag(msg.content);

    const [command, ...text_params] = parse_params(msg_content);
    const params = text_params.concat(attachments_as_params(msg));

    const author_is_admin = msg.member.hasPermission(Permissions.ALL);
    if (author_is_admin) {
        switch (command) {
            case cmd_prefix + cmd.Command.NewCustomCommand:
                return cmd.create_custom_command(params, msg, db_client);

            case cmd_prefix + cmd.Command.DeleteCustomCommand:
                return cmd.delete_custom_command(params, msg, db_client);

            case cmd_prefix + cmd.Command.SetupWelcomeMessage:
                return cmd.setup_welcome_message(params, msg, db_client);

            default: // non-admin command
                break;
        }
    }

    switch (command) {
        case cmd_prefix + cmd.Command.ListCustomCommands:
            return cmd.list_custom_commands(msg, db_client);

        case cmd_prefix + cmd.Command.Help:
            return cmd.show_help(msg, cmd_prefix);

        default: // custom made command, or unknown
            return cmd.reply_custom_command(msg_content, msg, db_client);
    }
}

async function server_join_handler(member: GuildMember | PartialGuildMember, db_client: Database) {
    return db_client.get_welcome_channel_id()
        .then(channel_id => {
            if (channel_id) {
                const channel_promise = member.client.channels.fetch(channel_id);
                const welcome_msg_promise = db_client.get_welcome_message();

                return Promise.all([channel_promise, welcome_msg_promise])
                    .then(([channel, welcome_msg]) => {
                        return (channel as TextChannel)
                            .send(replace_user_mention(welcome_msg, member));
                    });
            }
        });
}

export { ready_handler, message_handler, server_join_handler };