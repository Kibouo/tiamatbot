import { Command } from "./command";

function everyone_help_text(cmd_prefix: string): string {
    return `
\`${cmd_prefix}${Command.ListCustomCommands}\`: list all custom commands.
\`${cmd_prefix}${Command.Help}\`: show this message.`;
}

function admin_help_text(cmd_prefix: string): string {
    return `
\`${cmd_prefix}${Command.NewCustomCommand} command reply_msg\`: add a custom command to which the bot will reply. Multiple invocations will add alternative replies.
\`${cmd_prefix}${Command.DeleteCustomCommand} command\`: delete a custom command.
\`${cmd_prefix}${Command.SetupWelcomeMessage} channel msg\`: set the welcome channel and message for newly joined members.`;
}

function format_help_text(): string {
    return `
Group multi-word parameters with double-quotes (" "). Escaping these is possible (\\\\").
The keyword \`$user\` is replaced with a ping to the user which the bot responds to.`;
}

export { everyone_help_text, admin_help_text, format_help_text };