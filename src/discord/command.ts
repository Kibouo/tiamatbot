import { Database } from "../database";
import { Message, MessageEmbed } from "discord.js";
import { EmbedColor, replace_user_mention, random_int, extract_urls, attachments_as_params } from "../utils";
import { admin_help_text, everyone_help_text, format_help_text } from "./help_text";

enum Command {
    NewCustomCommand = "new-cmd",
    DeleteCustomCommand = "del-cmd",
    ListCustomCommands = "list",
    SetupWelcomeMessage = "set-greet",
    Help = "help",
}

async function create_custom_command(params: string[], msg: Message, db_client: Database) {
    const [custom_command, ...custom_reply_parts] = params;

    return db_client.set_custom_command(custom_command, custom_reply_parts.join(' '))
        .then(() => {
            return msg.channel.send(
                new MessageEmbed()
                    .setColor(EmbedColor.Good)
                    .setDescription(`Reply for custom command \`${custom_command}\` added!`)
            );
        })
        .then(() => console.log(`Replied to: ${Command.NewCustomCommand} [${params}]`))
        .catch(err => console.error(err));
}

async function reply_custom_command(custom_command: string, msg: Message, db_client: Database) {
    return db_client.get_custom_command(custom_command)
        .then(reply => {
            // no reply for the given command might exist
            if (reply.length !== 0) {
                const reply_idx = random_int(reply.length);
                const [text_reply, attachments] = extract_urls(reply[reply_idx]);

                return msg.channel.send(
                    replace_user_mention(text_reply, msg.member),
                    { files: attachments }
                );
            }
        })
        .then(reply_msg => {
            if (reply_msg) {
                console.log(`Replied to: ${custom_command} with [${reply_msg.content}] and attachments [${attachments_as_params(reply_msg)}]`);
            }
        })
        .catch(err => console.error(err));
}

async function delete_custom_command(params: string[], msg: Message, db_client: Database) {
    return db_client.delete_custom_command(params[0])
        .then(() => {
            return msg.channel.send(
                new MessageEmbed()
                    .setColor(EmbedColor.Good)
                    .setDescription("Custom command deleted.")
            );
        })
        .then(() => console.log(`Replied to: ${Command.DeleteCustomCommand} [${params}]`))
        .catch(err => console.error(err));
}

async function setup_welcome_message(params: string[], msg: Message, db_client: Database) {
    const [dirty_channel_id, ...welcome_message_parts] = params;
    // we only want the channel id (consisting of digits). When a user types #channel_name, this is converted to <#the_id>. Thus we need to discard the brackets and hash.
    const channel_id = dirty_channel_id.replace(/<|#|>/g, '');

    return db_client.set_welcome_channel_id(channel_id)
        .then(() => { return db_client.set_welcome_message(welcome_message_parts.join(' ')); })
        .then(() => {
            return msg.channel.send(
                new MessageEmbed()
                    .setColor(EmbedColor.Good)
                    // Discord auto-formats <#the_id> to a link
                    .setDescription(`Welcome message added! It will be sent in ${dirty_channel_id}`)
            );
        })
        .then(() => console.log(`Replied to: ${Command.SetupWelcomeMessage} [${params}]`))
        .catch(err => console.error(err));
}

async function list_custom_commands(msg: Message, db_client: Database) {
    return db_client.get_all_custom_commands()
        .then(commands => {
            if (commands.length === 0) {
                return msg.channel.send(
                    new MessageEmbed()
                        .setColor(EmbedColor.Bad)
                        .setDescription("No custom commands.")
                );
            } else {
                return msg.channel.send(
                    new MessageEmbed()
                        .setColor(EmbedColor.Default)
                        .setTitle("Custom commands")
                        .setDescription(commands.join(',\n'))
                );
            }
        })
        .then(() => console.log(`Replied to: ${Command.ListCustomCommands}`))
        .catch(err => console.error(err));
}

async function show_help(msg: Message, cmd_prefix: string) {
    return new Promise((_, __) => {
        return msg.channel.send(
            new MessageEmbed()
                .setColor(EmbedColor.Default)
                .setTitle("Default commands")
                .addFields(
                    { name: "Everyone", value: everyone_help_text(cmd_prefix) },
                    { name: "Admin only", value: admin_help_text(cmd_prefix) },
                    { name: "Formatting", value: format_help_text() }
                )
                .setFooter("Made by Kibouo#1698; https://gitlab.com/Kibouo/tiamatbot/")
                .setURL('https://gitlab.com/Kibouo/tiamatbot/')
        );
    })
        .then(() => console.log(`Replied to: ${Command.Help}`))
        .catch(err => console.error(err));
}

export {
    Command,
    create_custom_command,
    reply_custom_command,
    show_help,
    delete_custom_command,
    list_custom_commands,
    setup_welcome_message
};
